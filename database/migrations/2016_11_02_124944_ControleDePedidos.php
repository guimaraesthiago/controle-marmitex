<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ControleDePedidos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('controle_de_pedidos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('controle_de_clientes_id')->length(11);
            $table->integer('controle_de_entregadores_id')->length(11);
            $table->decimal('taxa_de_entrega', 10, 2)->length(20);
            $table->decimal('total', 10, 2)->length(20);
            $table->enum('status', [0,1,2,3]);
            $table->json('produtos');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('controle_de_pedidos');
    }
}
