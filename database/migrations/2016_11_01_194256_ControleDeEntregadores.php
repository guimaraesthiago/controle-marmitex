<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ControleDeEntregadores extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('controle_de_entregadores', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome_entregador')->length(100);
            $table->string('cpf')->length(45);
            $table->string('rg')->length(45);
            $table->string('celular')->length(45);
            $table->integer('empresa_terceirizada_id')->length(11);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('controle_de_entregadores');
    }
}
