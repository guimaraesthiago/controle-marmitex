<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ControleDeClientes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('controle_de_clientes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('cliente')->length(100);
            $table->string('telefone')->length(45);
            $table->string('endereco')->length(255);
            $table->string('ponto_referencia')->length(255)->nullable();
            $table->string('nascimento')->length(45);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('controle_de_clientes');
    }
}
