<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EmpresasTerceirizadas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empresas_terceirizadas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome_empresa')->length(100);
            $table->string('cnpj')->length(45);
            $table->string('endereco')->length(255);
            $table->string('telefone')->length(45);
            $table->string('email')->length(50);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('empresas_terceirizadas');
    }
}
