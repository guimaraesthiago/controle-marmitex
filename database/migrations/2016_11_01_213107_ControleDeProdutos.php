<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ControleDeProdutos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('controle_de_produtos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome_produto')->length(50);
            $table->string('descricao')->length(255)->nullable();
            $table->enum('tamanho', [0,1,2]);
            $table->decimal('custo', 10, 2)->length(20);
            $table->json('ingredientes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('controle_de_produtos');
    }
}
