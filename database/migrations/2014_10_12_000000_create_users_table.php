<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });

        //Usuário
        DB::table('users')->insert(
            array(
                'name' => 'Usuário',
                'email' => 'admin@admin.com',
                'password' => '$2y$10$oF1trz6qtkE/8dFVyjB1sOsFIHl3YPughN5clWvnqA0nlxPEHfVV2', //1234567
                'remember_token' => 'U0GemlHNoMfH4ny0yTryf21cG4tfjQBjYe1fhqhVJgYv4zylAND9aYKB37pC'
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
