<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return Redirect::to('login');
});

Auth::routes();

Route::get('/home', 'HomeController@index');

//Controle de Clientes
Route::group(['prefix' => 'controle-de-clientes'], function()
{
    Route::get('/', ['uses' => 'ControleDeClientesController@index']);
    Route::get('add', ['uses' => 'ControleDeClientesController@add']);
    Route::post('insert', ['uses' => 'ControleDeClientesController@insert']);
    Route::get('edit/{id}', ['uses' => 'ControleDeClientesController@edit']);
    Route::put('update/{id}', ['uses' => 'ControleDeClientesController@update']);
    Route::get('delete/{id}', ['uses' => 'ControleDeClientesController@delete']);
});

//Controle de Entregadores
Route::group(['prefix' => 'controle-de-entregadores'], function()
{
    Route::get('/', ['uses' => 'ControleDeEntregadoresController@index']);
    Route::get('add', ['uses' => 'ControleDeEntregadoresController@add']);
    Route::post('insert', ['uses' => 'ControleDeEntregadoresController@insert']);
    Route::get('edit/{id}', ['uses' => 'ControleDeEntregadoresController@edit']);
    Route::put('update/{id}', ['uses' => 'ControleDeEntregadoresController@update']);
    Route::get('delete/{id}', ['uses' => 'ControleDeEntregadoresController@delete']);
});

//Empresas Terceirizadas
Route::group(['prefix' => 'empresas-terceirizadas'], function()
{
    Route::get('/', ['uses' => 'EmpresasTerceirizadasController@index']);
    Route::get('add', ['uses' => 'EmpresasTerceirizadasController@add']);
    Route::post('insert', ['uses' => 'EmpresasTerceirizadasController@insert']);
    Route::get('edit/{id}', ['uses' => 'EmpresasTerceirizadasController@edit']);
    Route::put('update/{id}', ['uses' => 'EmpresasTerceirizadasController@update']);
    Route::get('delete/{id}', ['uses' => 'EmpresasTerceirizadasController@delete']);
});

//Controle de Ingredientes
Route::group(['prefix' => 'controle-de-ingredientes'], function()
{
    Route::get('/', ['uses' => 'ControleDeIngredientesController@index']);
    Route::get('add', ['uses' => 'ControleDeIngredientesController@add']);
    Route::post('insert', ['uses' => 'ControleDeIngredientesController@insert']);
    Route::get('edit/{id}', ['uses' => 'ControleDeIngredientesController@edit']);
    Route::put('update/{id}', ['uses' => 'ControleDeIngredientesController@update']);
    Route::get('delete/{id}', ['uses' => 'ControleDeIngredientesController@delete']);
});

//Controle de Produtos
Route::group(['prefix' => 'controle-de-produtos'], function()
{
    Route::get('/', ['uses' => 'ControleDeProdutosController@index']);
    Route::get('add', ['uses' => 'ControleDeProdutosController@add']);
    Route::post('insert', ['uses' => 'ControleDeProdutosController@insert']);
    Route::get('edit/{id}', ['uses' => 'ControleDeProdutosController@edit']);
    Route::put('update/{id}', ['uses' => 'ControleDeProdutosController@update']);
    Route::get('delete/{id}', ['uses' => 'ControleDeProdutosController@delete']);
});

//Controle de Pedidos
Route::group(['prefix' => 'controle-de-pedidos'], function()
{
    Route::get('/', ['uses' => 'ControleDePedidosController@index']);
    Route::get('add', ['uses' => 'ControleDePedidosController@add']);
    Route::post('insert', ['uses' => 'ControleDePedidosController@insert']);
    Route::get('edit/{id}', ['uses' => 'ControleDePedidosController@edit']);
    Route::put('update/{id}', ['uses' => 'ControleDePedidosController@update']);
    Route::get('delete/{id}', ['uses' => 'ControleDePedidosController@delete']);
});

