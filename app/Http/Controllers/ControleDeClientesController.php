<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Model\ControleDeClientes;


class ControleDeClientesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){

    	$clientes = ControleDeClientes::get();
        return view('controle-de-clientes/index', compact('clientes'));
    }

    public function add(){

        return view('controle-de-clientes/add');
    }

    public function insert(Request $request){
        $validate_messages = array('cliente.required' => 'O campo "Nome" é obrigatório.',
        						   'nascimento.required' => 'O campo "Nascimento" é obrigatório',
        						   'telefone.required' => 'O campo "Telefone" é obrigatório',
        						   'endereco.required' => 'O campo "Endereço" é obrigatório'
        						  );
        $validate_rules = array('cliente' => 'required',
        						'nascimento' => 'required',
        						'telefone' => 'required',
        						'endereco' => 'required'
        						);
        $this->validate($request, $validate_rules, $validate_messages);

        $input = $request->all();
        ControleDeClientes::create($input);

        return redirect('controle-de-clientes')->with('message', 'Salvo com sucesso!');
    }

    public function edit($id){
        $cliente = ControleDeClientes::find($id);
        return view('controle-de-clientes/edit',compact('cliente'));
    }

    public function update($id, Request $request){
       $validate_messages = array('cliente.required' => 'O campo "Nome" é obrigatório.',
        						   'nascimento.required' => 'O campo "Nascimento" é obrigatório',
        						   'telefone.required' => 'O campo "Telefone" é obrigatório',
        						   'endereco.required' => 'O campo "Endereço" é obrigatório'
        						  );
        $validate_rules = array('cliente' => 'required',
        						'nascimento' => 'required',
        						'telefone' => 'required',
        						'endereco' => 'required'
        						);
        $this->validate($request, $validate_rules, $validate_messages);

        $input = $request->all();
        ControleDeClientes::find($id)->fill($input)->save();

        return redirect('controle-de-clientes')->with('message', 'Atualizado com sucesso!');
    }

    public function delete($id){
        ControleDeClientes::find($id)->delete();

        return redirect('controle-de-clientes')->with('message', 'Deletado com sucesso!');
    }
}
