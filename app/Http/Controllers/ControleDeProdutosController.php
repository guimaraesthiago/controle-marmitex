<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Model\ControleDeIngredientes;
use App\Model\ControleDeProdutos;

class ControleDeProdutosController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){

    	$produtos = ControleDeProdutos::get();
        return view('controle-de-produtos/index', compact('produtos'));
    }

    public function add(){
    	$ingredientes = ControleDeIngredientes::get();
        return view('controle-de-produtos/add', compact('ingredientes'));
    }

    public function insert(Request $request){
        $validate_messages = array('nome_produto.required' => 'O campo "Nome do Produto" é obrigatório.',
        						   'descricao.required' => 'O campo "Descrição" é obrigatório.',
        						   'tamanho.required' => 'O campo "Tamanho" é obrigatório.',
        						   'custo.required' => 'O campo "Custo" é obrigatório.',
        						   'ingredientes.0.required' => 'O campo "Ingredientes" é obrigatório.'
        						  );
        $validate_rules = array('nome_produto' => 'required',
        						'descricao' => 'required',
        						'tamanho' => 'required',
        						'custo' => 'required',
        						'ingredientes.0' => 'required',
        						);
        $this->validate($request, $validate_rules, $validate_messages);

        $input = $request->all();
        $input["ingredientes"] = json_encode($input["ingredientes"]);
        ControleDeProdutos::create($input);

        return redirect('controle-de-produtos')->with('message', 'Salvo com sucesso!');
    }

    public function edit($id){
        $produto = ControleDeProdutos::find($id);
        $ingredientes = ControleDeIngredientes::get();
        $produto->ingredientes = json_decode($produto->ingredientes);
        return view('controle-de-produtos/edit',compact('produto', 'ingredientes'));
    }

    public function update($id, Request $request){
        $validate_messages = array('nome_produto.required' => 'O campo "Nome do Produto" é obrigatório.',
        						   'descricao.required' => 'O campo "Descrição" é obrigatório.',
        						   'tamanho.required' => 'O campo "Tamanho" é obrigatório.',
        						   'custo.required' => 'O campo "Custo" é obrigatório.',
        						   'ingredientes.0.required' => 'O campo "Ingredientes" é obrigatório.'
        						  );
        $validate_rules = array('nome_produto' => 'required',
        						'descricao' => 'required',
        						'tamanho' => 'required',
        						'custo' => 'required',
        						'ingredientes.0' => 'required',
        						);
        $this->validate($request, $validate_rules, $validate_messages);

        $input = $request->all();
        $input["ingredientes"] = json_encode($input["ingredientes"]);
        ControleDeProdutos::find($id)->fill($input)->save();

        return redirect('controle-de-produtos')->with('message', 'Atualizado com sucesso!');
    }

    public function delete($id){
        ControleDeProdutos::find($id)->delete();

        return redirect('controle-de-produtos')->with('message', 'Deletado com sucesso!');
    }
}
