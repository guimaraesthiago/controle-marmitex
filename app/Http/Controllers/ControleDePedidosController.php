<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Model\ControleDePedidos;
use App\Model\ControleDeClientes;
use App\Model\ControleDeEntregadores;
use App\Model\ControleDeProdutos;

class ControleDePedidosController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){

    	$pedidos = ControleDePedidos::leftjoin('controle_de_clientes', 'controle_de_clientes.id', '=', 'controle_de_pedidos.controle_de_clientes_id')
    								->select('controle_de_pedidos.*', 'controle_de_clientes.cliente')
    								->orderBy('controle_de_pedidos.id', 'ASC')
    								->get();
        return view('controle-de-pedidos/index', compact('pedidos'));
    }

    public function add(){
    	$clientes = ControleDeClientes::get();
    	$entregadores = ControleDeEntregadores::get();
    	$produtos = ControleDeProdutos::get();

        return view('controle-de-pedidos/add', compact('clientes', 'entregadores', 'produtos'));
    }

    public function insert(Request $request){
        $validate_messages = array(
        	'controle_de_clientes_id.required' => 'O campo "Cliente" é obrigatório.',
        	'controle_de_entregadores_id.required' => 'O campo "Entregador" é obrigatório.',
        	'taxa_de_entrega.required' => 'O campo "Taxa de Entrega" é obrigatório.',
        	'status.required' => 'O campo "Status" é obrigatório.',
        	'p_produto.0.required' => 'Selecionar ao menos um produto para o pedido é obrigatório.',
        	'p_quantidade.0.required' => 'Preecher a quantidade para cada produto selecionado é obrigatório.'
        	 );
        $validate_rules = array('controle_de_clientes_id' => 'required',
        						'controle_de_entregadores_id' => 'required',
        						'taxa_de_entrega' => 'required',
        						'status' => 'required',
        						'p_produto.0' => 'required',
        						'p_quantidade.0' => 'required'
        						);
        $this->validate($request, $validate_rules, $validate_messages);

        $input = $request->all();

        $total = 0;
        for($i=0; $i<count($input['p_produto']); $i++):
        	$produto = ControleDeProdutos::find($input['p_produto'][$i]);
        	$total += $produto->custo*$input['p_quantidade'][$i];
        endfor;

        $input["total"] = $total+$input["taxa_de_entrega"];
        $input["produtos"] = json_encode(
        						array(
        							"produtos_id" => $input["p_produto"], 
        							"quantidade" => $input["p_quantidade"]
    							)
							  );

        ControleDePedidos::create($input);

        return redirect('controle-de-pedidos')->with('message', 'Salvo com sucesso!');
    }

    public function edit($id){
        $clientes = ControleDeClientes::get();
    	$entregadores = ControleDeEntregadores::get();
    	$produtos = ControleDeProdutos::get();
    	$pedido = ControleDePedidos::find($id);
    	$pedido->produtos = json_decode($pedido->produtos);
        return view('controle-de-pedidos/edit',compact('clientes', 'entregadores', 'produtos', 'pedido'));
    }

    public function update($id, Request $request){
        $validate_messages = array(
        	'controle_de_clientes_id.required' => 'O campo "Cliente" é obrigatório.',
        	'controle_de_entregadores_id.required' => 'O campo "Entregador" é obrigatório.',
        	'taxa_de_entrega.required' => 'O campo "Taxa de Entrega" é obrigatório.',
        	'status.required' => 'O campo "Status" é obrigatório.',
        	'p_produto.0.required' => 'Selecionar ao menos um produto para o pedido é obrigatório.',
        	'p_quantidade.0.required' => 'Preecher a quantidade para cada produto selecionado é obrigatório.'
        	 );
        $validate_rules = array('controle_de_clientes_id' => 'required',
        						'controle_de_entregadores_id' => 'required',
        						'taxa_de_entrega' => 'required',
        						'status' => 'required',
        						'p_produto.0' => 'required',
        						'p_quantidade.0' => 'required'
        						);
        $this->validate($request, $validate_rules, $validate_messages);

        $input = $request->all();

        $total = 0;
        for($i=0; $i<count($input['p_produto']); $i++):
        	$produto = ControleDeProdutos::find($input['p_produto'][$i]);
        	$total += $produto->custo*$input['p_quantidade'][$i];
        endfor;

        $input["total"] = $total+$input["taxa_de_entrega"];
        $input["produtos"] = json_encode(
        						array(
        							"produtos_id" => $input["p_produto"], 
        							"quantidade" => $input["p_quantidade"]
    							)
							  );
        ControleDePedidos::find($id)->fill($input)->save();

        return redirect('controle-de-pedidos')->with('message', 'Atualizado com sucesso!');
    }

    public function delete($id){
        ControleDePedidos::find($id)->delete();

        return redirect('controle-de-pedidos')->with('message', 'Deletado com sucesso!');
    }
}
