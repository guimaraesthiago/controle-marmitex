<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Model\ControleDeIngredientes;

class ControleDeIngredientesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){

    	$ingredientes = ControleDeIngredientes::get();
        return view('controle-de-ingredientes/index', compact('ingredientes'));
    }

    public function add(){

        return view('controle-de-ingredientes/add');
    }

    public function insert(Request $request){
        $validate_messages = array('ingrediente.required' => 'O campo "Ingrediente" é obrigatório.');
        $validate_rules = array('ingrediente' => 'required');
        $this->validate($request, $validate_rules, $validate_messages);

        $input = $request->all();
        ControleDeIngredientes::create($input);

        return redirect('controle-de-ingredientes')->with('message', 'Salvo com sucesso!');
    }

    public function edit($id){
        $ingrediente = ControleDeIngredientes::find($id);
        return view('controle-de-ingredientes/edit',compact('ingrediente'));
    }

    public function update($id, Request $request){
        $validate_messages = array('ingrediente.required' => 'O campo "Ingrediente" é obrigatório.');
        $validate_rules = array('ingrediente' => 'required');
        $this->validate($request, $validate_rules, $validate_messages);

        $input = $request->all();
        ControleDeIngredientes::find($id)->fill($input)->save();

        return redirect('controle-de-ingredientes')->with('message', 'Atualizado com sucesso!');
    }

    public function delete($id){
        ControleDeIngredientes::find($id)->delete();

        return redirect('controle-de-ingredientes')->with('message', 'Deletado com sucesso!');
    }
}
