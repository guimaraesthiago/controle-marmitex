<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Model\ControleDeEntregadores;
use App\Model\EmpresasTerceirizadas;

class ControleDeEntregadoresController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){

    	$entregadores = ControleDeEntregadores::
    					leftjoin('empresas_terceirizadas', 'controle_de_entregadores.empresa_terceirizada_id', '=', 'empresas_terceirizadas.id')
    					->select('controle_de_entregadores.*', 'empresas_terceirizadas.nome_empresa')
    					->get();

        return view('controle-de-entregadores/index', compact('entregadores'));
    }

    public function add(){
    	$empresas_terceirizadas = EmpresasTerceirizadas::get();
        return view('controle-de-entregadores/add', compact('empresas_terceirizadas'));
    }

    public function insert(Request $request){
        $validate_messages = array('nome_entregador.required' => 'O campo "Nome do Entregador" é obrigatório.',
        						   'rg.required' => 'O campo "RG" é obrigatório',
        						   'cpf.required' => 'O campo "CPF" é obrigatório',
        						   'celular.required' => 'O campo "Celular" é obrigatório',
        						   'empresa_terceirizada_id.required' => 'O campo "Empresa Terceirizada" é obrigatório'
        						  );
        $validate_rules = array('nome_entregador' => 'required',
        						'rg' => 'required',
        						'cpf' => 'required',
        						'celular' => 'required',
        						'empresa_terceirizada_id' => 'required'
        						);
        $this->validate($request, $validate_rules, $validate_messages);

        $input = $request->all();
        ControleDeEntregadores::create($input);

        return redirect('controle-de-entregadores')->with('message', 'Salvo com sucesso!');
    }

    public function edit($id){
        $entregador = ControleDeEntregadores::find($id);
        $empresas_terceirizadas = EmpresasTerceirizadas::get();
        return view('controle-de-entregadores/edit',compact('entregador', 'empresas_terceirizadas'));
    }

    public function update($id, Request $request){
       $validate_messages = array('nome_entregador.required' => 'O campo "Nome do Entregador" é obrigatório.',
        						   'rg.required' => 'O campo "RG" é obrigatório',
        						   'cpf.required' => 'O campo "CPF" é obrigatório',
        						   'celular.required' => 'O campo "Celular" é obrigatório',
        						   'empresa_terceirizada_id.required' => 'O campo "Empresa Terceirizada" é obrigatório'
        						  );
        $validate_rules = array('nome_entregador' => 'required',
        						'rg' => 'required',
        						'cpf' => 'required',
        						'celular' => 'required',
        						'empresa_terceirizada_id' => 'required'
        						);
        $this->validate($request, $validate_rules, $validate_messages);

        $input = $request->all();
        ControleDeEntregadores::find($id)->fill($input)->save();

        return redirect('controle-de-entregadores')->with('message', 'Atualizado com sucesso!');
    }

    public function delete($id){
        ControleDeEntregadores::find($id)->delete();

        return redirect('controle-de-entregadores')->with('message', 'Deletado com sucesso!');
    }
}
