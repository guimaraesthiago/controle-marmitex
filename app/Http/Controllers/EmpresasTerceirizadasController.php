<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Model\EmpresasTerceirizadas;

class EmpresasTerceirizadasController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $empresas = EmpresasTerceirizadas::get();
        return view('empresas-terceirizadas/index', compact('empresas'));
    }

    public function add(){
    	$empresas_terceirizadas = EmpresasTerceirizadas::get();
        return view('empresas-terceirizadas/add', compact('empresas_terceirizadas'));
    }

    public function insert(Request $request){
        $validate_messages = array('nome_empresa.required' => 'O campo "Nome da Empresa" é obrigatório.',
        						   'cnpj.required' => 'O campo "CNPJ" é obrigatório',
        						   'email.required' => 'O campo "Email" é obrigatório',
                                   'telefone.required' => 'O campo "Telefone" é obrigatório',
        						   'endereco.required' => 'O campo "Endereço" é obrigatório'
        						  );
        $validate_rules = array('nome_empresa' => 'required',
        						'cnpj' => 'required',
        						'email' => 'required',
                                'telefone' => 'required',
        						'endereco' => 'required'
        						);
        $this->validate($request, $validate_rules, $validate_messages);

        $input = $request->all();
        EmpresasTerceirizadas::create($input);

        return redirect('empresas-terceirizadas')->with('message', 'Salvo com sucesso!');
    }

    public function edit($id){
        $empresa = EmpresasTerceirizadas::find($id);
        return view('empresas-terceirizadas/edit',compact('empresa'));
    }

    public function update($id, Request $request){
        $validate_messages = array('nome_empresa.required' => 'O campo "Nome da Empresa" é obrigatório.',
                                   'cnpj.required' => 'O campo "CNPJ" é obrigatório',
                                   'email.required' => 'O campo "Email" é obrigatório',
                                   'telefone.required' => 'O campo "Telefone" é obrigatório',
                                   'endereco.required' => 'O campo "Endereço" é obrigatório'
                                  );
        $validate_rules = array('nome_empresa' => 'required',
                                'cnpj' => 'required',
                                'email' => 'required',
                                'telefone' => 'required',
                                'endereco' => 'required'
                                );
        $this->validate($request, $validate_rules, $validate_messages);

        $input = $request->all();
        EmpresasTerceirizadas::find($id)->fill($input)->save();

        return redirect('empresas-terceirizadas')->with('message', 'Atualizado com sucesso!');
    }

    public function delete($id){
        EmpresasTerceirizadas::find($id)->delete();

        return redirect('empresas-terceirizadas')->with('message', 'Deletado com sucesso!');
    }
}
