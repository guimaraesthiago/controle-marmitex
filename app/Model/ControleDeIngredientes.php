<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ControleDeIngredientes extends Model
{
    protected $fillable = [
			'id',
			'ingrediente'
    ];
}
