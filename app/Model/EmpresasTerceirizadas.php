<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class EmpresasTerceirizadas extends Model
{
	protected $fillable = [
			'id',
			'nome_empresa',
			'cnpj',
			'endereco',
			'telefone',
			'email'
    ];

}
