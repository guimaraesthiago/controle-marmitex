<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ControleDePedidos extends Model
{
    protected $fillable = [
			'id',
			'controle_de_clientes_id',
			'controle_de_entregadores_id',
			'taxa_de_entrega',
			'total',
			'status',
			'produtos'
    ];
}
