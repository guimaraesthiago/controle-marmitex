<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ControleDeClientes extends Model
{
    protected $fillable = [
			'id',
			'cliente',
			'telefone',
			'endereco',
			'ponto_referencia',
			'nascimento'
    ];
}
