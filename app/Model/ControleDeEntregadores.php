<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ControleDeEntregadores extends Model
{
    protected $fillable = [
			'id',
			'nome_entregador',
			'cpf',
			'rg',
			'celular',
			'empresa_terceirizada_id'
    ];
}
