<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ControleDeProdutos extends Model
{
	protected $fillable = [
			'id',
			'nome_produto',
			'descricao',
			'tamanho',
			'custo',
			'ingredientes'
    ];

}
