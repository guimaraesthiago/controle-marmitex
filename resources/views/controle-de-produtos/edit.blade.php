@extends('layouts.app')

@section('content')

{!! 
Form::open(array('action' => array('ControleDeProdutosController@update', $produto->id), 'method' => 'put'))
!!}

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                  <div class="btn-group pull-right">
                      <button type="submit" class="btn btn-success">
                        Salvar
                      </button>
                      <a href="/controle-de-produtos" class="btn btn-default">
                        Cancelar
                      </a>
                  </div>
                  <h4>Editar Produto</h4>
                </div>
                <div class="panel-body ">

                  @if($errors->any())
                      <div class="alert alert-danger">
                          @foreach($errors->all() as $error)
                              <p>{{ $error }}</p>
                          @endforeach
                      </div>
                  @endif

                  <div class="form-horizontal">
                    <div class="form-group">
                      <label for="nome_produto" class="col-sm-2 control-label">Nome do Produto</label>
                      <div class="col-sm-6">
                        <input type="text" class="form-control" id="nome_produto" placeholder="Nome do Produto" name="nome_produto" value="{{ $produto->nome_produto }}">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="descricao" class="col-sm-2 control-label">Descrição</label>
                      <div class="col-sm-5">
                        <textarea class="form-control" id="descricao" placeholder="Descrição" name="descricao" rows="3">{{ $produto->descricao }}</textarea>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="tamanho" class="col-sm-2 control-label">Tamanho</label>
                      <div class="col-sm-3">
                        <select class="form-control" name="tamanho" id="tamanho">
                            <option value="0" {{ $produto->tamanho == 0 ? 'selected' : '' }}>Pequeno</option>
                            <option value="1" {{ $produto->tamanho == 1 ? 'selected' : '' }}>Médio</option>
                            <option value="2" {{ $produto->tamanho == 2 ? 'selected' : '' }}>Grande</option>
                        </select>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="custo" class="col-sm-2 control-label">Custo</label>
                      <div class="col-sm-5">
                        <input type="text" class="form-control moeda" id="custo" placeholder="Custo" name="custo" value="{{ $produto->custo }}">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="ingredientes" class="col-sm-2 control-label">Ingredientes</label>
                      <div class="col-sm-5">
                        <select class="form-control multiple-select" name="ingredientes[]" id="ingredientes" multiple>
                            @foreach($ingredientes as $value)
                              @if(in_array($value->id, $produto->ingredientes))
                                <option value="{{ $value->id }}" selected>{{ $value->ingrediente }}</option>
                              @else
                                <option value="{{ $value->id }}">{{ $value->ingrediente }}</option>
                              @endif
                            @endforeach
                        </select>
                      </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}

@endsection
