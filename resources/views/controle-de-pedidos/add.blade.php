@extends('layouts.app')

@section('content')

{!! 
Form::open(array('action' => array('ControleDePedidosController@insert'), 'method' => 'post'))
!!}

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
              <div class="panel-heading">
                <div class="btn-group pull-right">
                    <button type="submit" class="btn btn-success">
                      Salvar
                    </button>
                    <a href="/controle-de-pedidos" class="btn btn-default">
                      Cancelar
                    </a>
                </div>
                <h4>Novo Pedido</h4>
              </div>
              <div class="panel-body">

                @if($errors->any())
                    <div class="alert alert-danger">
                        @foreach($errors->all() as $error)
                            <p>{{ $error }}</p>
                        @endforeach
                    </div>
                @endif
          
                <div class="form-horizontal">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="controle_de_clientes_id" class="col-sm-3 control-label">Cliente</label>
                      <div class="col-sm-8">
                        <select class="form-control" name="controle_de_clientes_id" id="controle_de_clientes_id">
                          <option value="">Selecione uma Opção</option>
                          @foreach($clientes as $value)
                            @if(old('controle_de_clientes_id') == $value->id)
                              <option value="{{ $value->id }}" selected>{{ $value->cliente }}</option>
                            @else
                              <option value="{{ $value->id }}">{{ $value->cliente }}</option>
                            @endif
                          @endforeach
                        </select>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="controle_de_entregadores_id" class="col-sm-3 control-label">Entregador</label>
                      <div class="col-sm-8">
                        <select class="form-control" name="controle_de_entregadores_id" id="controle_de_entregadores_id">
                          <option value="">Selecione uma Opção</option>
                          @foreach($entregadores as $value)
                            @if(old('controle_de_entregadores_id') == $value->id)
                              <option value="{{ $value->id }}" selected>{{ $value->nome_entregador }}</option>
                            @else
                              <option value="{{ $value->id }}">{{ $value->nome_entregador }}</option>
                            @endif
                          @endforeach
                        </select>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="taxa_de_entrega" class="col-sm-3 control-label">Taxa de Entrega</label>
                      <div class="col-sm-6">
                        <input type="text" class="form-control moeda" id="taxa_de_entrega" placeholder="Taxa de Entrega" name="taxa_de_entrega" value="{{ old('taxa_de_entrega') }}">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="status" class="col-sm-3 control-label">Status</label>
                      <div class="col-sm-6">
                        <select class="form-control" name="status" id="status">
                            <option value="0" {{ old('status') == 0 ? 'selected' : '' }}>Pendente</option>
                            <option value="1" {{ old('status') == 1 ? 'selected' : '' }}>Em trânsito</option>
                            <option value="2" {{ old('status') == 2 ? 'selected' : '' }}>Cancelado</option>
                            <option value="2" {{ old('status') == 3 ? 'selected' : '' }}>Entregue</option>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <h4 class="h4">Produtos do Pedido</h4>
                    <table class="table table-bordered table-hover table-striped "> 
                      <thead> 
                        <tr> 
                          <th>Produto</th> 
                          <th class="col-md-1">Quantidade</th> 
                          <th class="col-md-1"></th>
                        </tr> 
                      </thead> 
                      <tbody>
                        <tr>
                          <td>
                            <select class="form-control" name="p_produto[]">
                              <option value="">Selecione uma Opção</option>
                              @foreach($produtos as $value)
                                @if(old('p_produto.0') == $value->id)
                                  <option value="{{ $value->id }}" selected>{{ $value->nome_produto }}</option>
                                @else
                                  <option value="{{ $value->id }}">{{ $value->nome_produto }}</option>
                                @endif
                              @endforeach
                            </select>
                          </td>
                          <td>
                            <input type="text" class="form-control" name="p_quantidade[]" value="{{ old('p_quantidade.0') }}">
                          </td>
                          <td>
                            <button type="button" class="btn btn-success" id="novoProduto">
                              <i class="fa fa-plus" aria-hidden="true"></i>
                            </button>
                          </td>
                        </tr>

                        @if(count(old('p_produto')) > 1)
                          @for($i=1; $i<count(old('p_produto')); $i++)
                            <tr>
                              <td>
                                <select class="form-control" name="p_produto[]">
                                  <option value="">Selecione uma Opção</option>
                                  @foreach($produtos as $value)
                                    @if(old('p_produto.'.$i.'') == $value->id)
                                      <option value="{{ $value->id }}" selected>{{ $value->nome_produto }}</option>
                                    @else
                                      <option value="{{ $value->id }}">{{ $value->nome_produto }}</option>
                                    @endif
                                  @endforeach
                                </select>
                              </td>
                              <td>
                                <input type="text" class="form-control" name="p_quantidade[]" value="{{ old('p_quantidade.'.$i.'') }}">
                              </td>
                              <td>
                                <button type="button" class="btn btn-danger removeLinha">
                                  <i class="fa fa-trash" aria-hidden="true"></i>
                                </button>
                              </td>
                            </tr>
                          @endfor
                        @endif
                        
                      </tbody>
                    </table>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}

@endsection
