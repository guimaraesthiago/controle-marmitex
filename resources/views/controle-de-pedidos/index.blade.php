@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                  <div class="btn-group pull-right">
                      <a href="home" class="btn btn-default">
                        Voltar ao menu
                      </a>
                      <a href="controle-de-pedidos/add" class="btn btn-success" data-toggle="tooltip" data-placement="top" title="Cadastrar">
                        <i class="fa fa-plus" aria-hidden="true"></i>
                      </a>
                  </div>
                  <h4>Controle de Pedidos</h4>
                </div>
                <div class="panel-body ">
                  @if (session('message'))
                    <div class="alert alert-success">
                       <p>{{ session('message') }}</p>
                    </div>
                  @endif 
                  <div class="table-responsive">
                    <table class="table table-bordered table-hover table-striped "> 
                      <thead> 
                        <tr> 
                          <th class="col-md-1 col-sm-1 col-xs-1">#</th> 
                          <th class="col-md-1 col-sm-1 col-xs-1">Data do Ped.</th> 
                          <th class="col-md-3 visible-md visible-lg">Cliente</th> 
                          <th class="col-md-2 col-sm-2 col-xs-2">Valor</th>
                          <th class="col-md-2 col-sm-2 col-xs-2">Status</th>
                          <th class="col-md-1 col-sm-2 col-xs-1">Ações</th> 
                        </tr> 
                      </thead> 
                      <tbody> 
                        @foreach($pedidos as $value)
                          <tr>
                            <td>{{ $value->id }}</td>
                            <td>{{ date('d/m/Y', strtotime($value->created_at)) }}</td> 
                            <td class="visible-md visible-lg">{{ $value->cliente }}</td>
                            <td>R$ {{ number_format($value->total,2,".",",") }}</td>
                            <td>
                              @if($value->status == 0)
                                Pendente
                              @elseif($value->status == 1)
                                Em Trânsito
                              @elseif($value->status == 2)
                                Cancelado
                              @else
                                Entregue
                              @endif
                            </td>
                            <td>
                              <a href="controle-de-pedidos/edit/{{ $value->id }}" class="btn btn-primary btn-xs" data-toggle="tooltip" data-placement="top" title="Editar">
                                <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                              </a>
                              <a href="controle-de-pedidos/delete/{{ $value->id }}" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="Apagar">
                                <i class="fa fa-trash-o" aria-hidden="true"></i>
                              </a>
                            </td>
                          </tr>
                        @endforeach
                      </tbody> 
                    </table>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
