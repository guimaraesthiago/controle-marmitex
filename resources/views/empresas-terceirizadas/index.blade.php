@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                  <div class="btn-group pull-right">
                      <a href="home" class="btn btn-default">
                        Voltar ao menu
                      </a>
                      <a href="empresas-terceirizadas/add" class="btn btn-success" data-toggle="tooltip" data-placement="top" title="Cadastrar">
                        <i class="fa fa-plus" aria-hidden="true"></i>
                      </a>
                  </div>
                  <h4>Lista de Empresas Terceirizadas</h4>
                </div>
                <div class="panel-body ">
                  @if (session('message'))
                    <div class="alert alert-success">
                       <p>{{ session('message') }}</p>
                    </div>
                  @endif 
                  <div class="table-responsive">
                    <table class="table table-bordered table-hover table-striped "> 
                      <thead> 
                        <tr> 
                          <th class="col-md-1 col-sm-1 col-xs-1">#</th> 
                          <th class="col-md-3">Nome da Empresa</th>
                          <th class="col-md-2 visible-md visible-lg">CNPJ</th> 
                          <th class="col-md-2">Telefone</th> 
                          <th class="col-md-3 visible-md visible-lg">Endereço</th>
                          <th class="col-md-1 col-sm-2 col-xs-1">Ações</th> 
                        </tr> 
                      </thead> 
                      <tbody> 
                        @foreach($empresas as $value)
                          <tr>
                            <td>{{ $value->id }}</td>
                            <td>{{ $value->nome_empresa }}</td> 
                            <td class="visible-md visible-lg">{{ $value->cnpj }}</td> 
                            <td>{{ $value->telefone }}</td> 
                            <td class="visible-md visible-lg">{{ $value->endereco }}</td>
                            <td>
                              <a href="empresas-terceirizadas/edit/{{ $value->id }}" class="btn btn-primary btn-xs" data-toggle="tooltip" data-placement="top" title="Editar">
                                <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                              </a>
                              <a href="empresas-terceirizadas/delete/{{ $value->id }}" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="Apagar">
                                <i class="fa fa-trash-o" aria-hidden="true"></i>
                              </a>
                            </td>
                          </tr>
                        @endforeach
                      </tbody> 
                    </table>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
