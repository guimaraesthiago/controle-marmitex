@extends('layouts.app')

@section('content')

{!! 
Form::open(array('action' => array('EmpresasTerceirizadasController@insert'), 'method' => 'post'))
!!}

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                  <div class="btn-group pull-right">
                      <button type="submit" class="btn btn-success">
                        Salvar
                      </button>
                      <a href="/empresas-terceirizadas" class="btn btn-default">
                        Cancelar
                      </a>
                  </div>
                  <h4>Nova Empresa Terceirizada</h4>
                </div>
                <div class="panel-body ">

                  @if($errors->any())
                      <div class="alert alert-danger">
                          @foreach($errors->all() as $error)
                              <p>{{ $error }}</p>
                          @endforeach
                      </div>
                  @endif

                  <div class="form-horizontal">
                    <div class="form-group">
                      <label for="nome_empresa" class="col-sm-2 control-label">Nome da Empresa</label>
                      <div class="col-sm-6">
                        <input type="text" class="form-control" id="nome_empresa" placeholder="Nome da Empresa" name="nome_empresa" value="{{ old('nome_empresa') }}">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="cnpj" class="col-sm-2 control-label">CNPJ</label>
                      <div class="col-sm-3">
                        <input type="text" class="form-control cnpj" id="cnpj" placeholder="CNPJ" name="cnpj" value="{{ old('cnpj') }}">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="telefone" class="col-sm-2 control-label">Telefone</label>
                      <div class="col-sm-3">
                        <input type="telefone" class="form-control telefone" id="telefone" placeholder="telefone" name="telefone" value="{{ old('telefone') }}">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="email" class="col-sm-2 control-label">Email</label>
                      <div class="col-sm-3">
                        <input type="email" class="form-control" id="email" placeholder="Email" name="email" value="{{ old('email') }}">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="endereco" class="col-sm-2 control-label">Endereço</label>
                      <div class="col-sm-5">
                        <textarea class="form-control" id="endereco" placeholder="Endereço" name="endereco" rows="3">{{ old('endereco') }}</textarea>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}

@endsection
