@extends('layouts.app')

@section('content')

{!! 
Form::open(array('action' => array('ControleDeIngredientesController@update', $ingrediente->id), 'method' => 'put'))
!!}

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                  <div class="btn-group pull-right">
                      <button type="submit" class="btn btn-success">
                        Salvar
                      </button>
                      <a href="/controle-de-ingredientes" class="btn btn-default">
                        Cancelar
                      </a>
                  </div>
                  <h4>Editar Ingrediente</h4>
                </div>
                <div class="panel-body ">

                  @if($errors->any())
                      <div class="alert alert-danger">
                          @foreach($errors->all() as $error)
                              <p>{{ $error }}</p>
                          @endforeach
                      </div>
                  @endif

                  <div class="form-horizontal">
                    <div class="form-group">
                      <label for="ingrediente" class="col-sm-2 control-label">Ingrediente</label>
                      <div class="col-sm-3">
                        <input type="text" class="form-control" id="ingrediente" placeholder="Ingrediente" name="ingrediente" value="{{ $ingrediente->ingrediente }}">
                      </div>
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}

@endsection
