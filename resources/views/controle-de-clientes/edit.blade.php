@extends('layouts.app')

@section('content')

{!! 
Form::open(array('action' => array('ControleDeClientesController@update', $cliente->id), 'method' => 'put'))
!!}

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                  <div class="btn-group pull-right">
                      <button type="submit" class="btn btn-success">
                        Atualizar
                      </button>
                      <a href="/controle-de-clientes" class="btn btn-default">
                        Cancelar
                      </a>
                  </div>
                  <h4>Editar Cliente</h4>
                </div>
                <div class="panel-body ">

                  @if($errors->any())
                      <div class="alert alert-danger">
                          @foreach($errors->all() as $error)
                              <p>{{ $error }}</p>
                          @endforeach
                      </div>
                  @endif

                  <div class="form-horizontal">
                    <div class="form-group">
                      <label for="cliente" class="col-sm-2 control-label">Nome</label>
                      <div class="col-sm-6">
                        <input type="text" class="form-control" id="cliente" placeholder="Nome" name="cliente" value="{{ $cliente->cliente }}">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="nascimento" class="col-sm-2 control-label">Nascimento</label>
                      <div class="col-sm-3">
                        <input type="text" class="form-control data" id="nascimento" placeholder="Nascimento" name="nascimento" value="{{ $cliente->nascimento }}">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="telefone" class="col-sm-2 control-label">Telefone</label>
                      <div class="col-sm-3">
                        <input type="text" class="form-control telefone" id="telefone" placeholder="Telefone" name="telefone" value="{{ $cliente->telefone }}">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="endereco" class="col-sm-2 control-label">Endereço</label>
                      <div class="col-sm-5">
                        <textarea class="form-control" id="endereco" placeholder="Endereço" name="endereco" rows="3">{{ $cliente->endereco }}</textarea>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="ponto_referencia" class="col-sm-2 control-label">Ponto de Refrência</label>
                      <div class="col-sm-5">
                        <input type="text" class="form-control" id="ponto_referencia" placeholder="Ponto de Referência" name="ponto_referencia" value="{{ $cliente->ponto_referencia }}">
                      </div>
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}

@endsection
