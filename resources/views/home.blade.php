@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading"><h4>Menu</h4></div>

                <div class="panel-body custom-menu">
                    <div class="row">
                      <div class="col-md-4">
                        <a href="/controle-de-clientes">
                          <i class="fa fa-users" aria-hidden="true"></i>
                          <h3>Controle de Clientes</h3>
                        </a>
                      </div>
                      <div class="col-md-4">
                        <a href="/controle-de-entregadores">
                          <i class="fa fa-motorcycle" aria-hidden="true"></i>
                          <h3>Controle de Entregadores</h3>
                        </a>
                      </div>
                      <div class="col-md-4">
                        <a href="/empresas-terceirizadas">
                          <i class="fa fa-suitcase" aria-hidden="true"></i>
                          <h3>Empresas Terceirizadas</h3>
                        </a>
                      </div>
                    </div>
                    <br clear="clearfix">
                    <div class="row">
                      <div class="col-md-4">
                        <a href="/controle-de-ingredientes">
                          <i class="fa fa-shopping-basket" aria-hidden="true"></i>
                          <h3>Controle de Ingredientes</h3>
                        </a>
                      </div>
                      <div class="col-md-4">
                        <a href="/controle-de-produtos">
                          <i class="fa fa-barcode" aria-hidden="true"></i>
                          <h3>Controle de Produtos</h3>
                        </a>
                      </div>
                      <div class="col-md-4">
                        <a href="/controle-de-pedidos">
                          <i class="fa fa-clipboard" aria-hidden="true"></i>
                          <h3>Controle de Pedidos</h3>
                        </a>
                      </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
