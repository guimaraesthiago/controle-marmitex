@extends('layouts.app')

@section('content')

{!! 
Form::open(array('action' => array('ControleDeEntregadoresController@update', $entregador->id), 'method' => 'put'))
!!}

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                  <div class="btn-group pull-right">
                      <button type="submit" class="btn btn-success">
                        Atualizar
                      </button>
                      <a href="/controle-de-entregadores" class="btn btn-default">
                        Cancelar
                      </a>
                  </div>
                  <h4>Editar Entregador</h4>
                </div>
                <div class="panel-body ">

                  @if($errors->any())
                      <div class="alert alert-danger">
                          @foreach($errors->all() as $error)
                              <p>{{ $error }}</p>
                          @endforeach
                      </div>
                  @endif

                  <div class="form-horizontal">
                    <div class="form-group">
                      <label for="nome_entregador" class="col-sm-2 control-label">Nome do Entregador</label>
                      <div class="col-sm-6">
                        <input type="text" class="form-control" id="nome_entregador" placeholder="Nome do Entregador" name="nome_entregador" value="{{ $entregador->nome_entregador }}">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="rg" class="col-sm-2 control-label">RG</label>
                      <div class="col-sm-2">
                        <input type="text" class="form-control" id="rg" placeholder="RG" name="rg" value="{{ $entregador->rg }}" maxlength="15">
                      </div>
                       <label for="cpf" class="col-sm-1 control-label">CPF</label>
                      <div class="col-sm-3">
                        <input type="text" class="form-control telefone cpf" id="cpf" placeholder="CPF" name="cpf" value="{{ $entregador->cpf }}">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="celular" class="col-sm-2 control-label">Celular</label>
                      <div class="col-sm-5">
                        <input type="text" class="form-control telefone" id="celular" placeholder="Celular" name="celular" value="{{ $entregador->celular }}">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="empresa_terceirizada_id" class="col-sm-2 control-label">Empresa Terceirizada</label>
                      <div class="col-sm-5">
                        <select class="form-control" name="empresa_terceirizada_id" id="empresa_terceirizada_id">
                          <option value="">Selecione uma empresa</option>
                          @foreach($empresas_terceirizadas as $value)
                            @if($entregador->empresa_terceirizada_id == $value->id)
                              <option value="{{ $value->id }}" selected>{{ $value->nome_empresa }}</option>
                            @else
                              <option value="{{ $value->id }}">{{ $value->nome_empresa }}</option>
                            @endif
                          @endforeach
                        </select>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}

@endsection
