$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})

$(".multiple-select").select2({
	placeholder: 'Ingredientes'
});

var TelefoneNoveDigitos = function (val) {
  return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
},
spOptions = {
  onKeyPress: function(val, e, field, options) {
      field.mask(TelefoneNoveDigitos.apply({}, arguments), options);
    }
};

$('.telefone').mask(TelefoneNoveDigitos, spOptions);
$('.data').mask('00/00/0000');
$('.cpf').mask('000.000.000-00');
$('.cnpj').mask('00.000.000/0000-00');
$(".moeda").maskMoney({allowNegative: true, thousands:'', decimal:'.', affixesStay: false});

$(document).on('click', '#novoProduto', function() {
	var deleteButton = '<button type="button" class="btn btn-danger removeLinha">'+
                          '<i class="fa fa-trash" aria-hidden="true"></i>'+
                        '</button>';
	var tr = $('.table tbody tr:first-child').clone();
	$(tr).find('td:last-child').html(deleteButton);
  $(tr).find('input').val('');
  $(tr).find('select').val('');
	$('.table tbody').append(tr);
});

$(document).on('click', '.removeLinha', function() {
	$(this).closest('tr').remove();
});